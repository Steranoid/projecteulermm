//This file is part of steranoid_projecteuler.

//steranoid_projecteuler is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//steranoid_projecteuler is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with steranoid_projecteuler.  If not, see <http://www.gnu.org/licenses/>.

#ifndef COORDINATES_HPP
#define COORDINATES_HPP

namespace steranoid {
	template <typename T>
	class coordinates {
	public:
		using dimension = T;

		//Construct and assign
		constexpr coordinates(dimension x, dimension y) : _x(x), _y(y) {}
		constexpr coordinates() : coordinates(dimension(), dimension()) {}

		//Copy
		coordinates(coordinates const &) = default;
		coordinates & operator=(coordinates const &) = default;

		//Move
		coordinates(coordinates &&) = default;
		coordinates & operator=(coordinates &&) = default;

		//Compare
		constexpr bool operator==(coordinates coords) const {
			return _x == coords._x and _y == coords._y;
		}
		constexpr bool operator!=(coordinates coords) const {
			return not operator==(coords);
		}

		//Add or substract
		constexpr coordinates operator+(coordinates coords) const {
			return coordinates(_x+coords._x, _y+coords._y);
		}
		constexpr coordinates operator-(coordinates coords) const {
			return coordinates(_x-coords._x, _y-coords._y);
		}
		constexpr coordinates operator*(unsigned long long i) const {
			return coordinates(i*_x, i*_y);
		}
		constexpr friend coordinates operator*(unsigned long long i, coordinates coords) {
			return coords*i;
		}

		//Add or substract and then copy
		coordinates & operator+=(coordinates const & coords) {
			return operator=(operator+(coords));
		}
		coordinates & operator-=(coordinates const & coords) {
			return operator=(operator-(coords));
		}

		constexpr dimension x() const {
			return _x;
		}
		constexpr dimension y() const {
			return _y;
		}

	private:
		dimension _x;
		dimension _y;
	};

	template <typename T>
	struct coordinates_sorter {
		bool operator()(coordinates<T> const & a, coordinates<T> const & b) const {
			return a.y() < b.y() or (a.y() == b.y() and a.x() < b.x());
		}
	};
}

#endif
