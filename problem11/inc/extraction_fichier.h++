#ifndef EXTRACTION_FICHIER_HPP
#define EXTRACTION_FICHIER_HPP

#include <map>
#include <fstream>
#include "coordinates.h++"

namespace steranoid {
	auto extrait_depuis_fichier(std::ifstream & ifs) -> std::map<coordinates<unsigned short>, unsigned long long, steranoid::coordinates_sorter<unsigned short>>;
}

#endif
