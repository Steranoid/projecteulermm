#ifndef PRODUIT_ALIGNES_HPP
#define PRODUIT_ALIGNES_HPP

#include <fstream>

namespace steranoid {
	auto produit_alignes(std::ifstream & ifs, unsigned long size = 4) -> unsigned long long;
}

#endif
