#ifndef PRODUIT_LISTE_HPP
#define PRODUIT_LISTE_HPP

#include <list>

namespace steranoid {
	auto produit_liste(std::list<unsigned long long> const & q) -> unsigned long long;
}

#endif
