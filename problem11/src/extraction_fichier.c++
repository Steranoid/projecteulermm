#include "extraction_fichier.h++"

using std::map;
using steranoid::coordinates;
using steranoid::coordinates_sorter;
using coordinates_type = coordinates<unsigned short>;
using nombre_tab = map<coordinates_type, unsigned long long, coordinates_sorter<unsigned short>>;
using std::ifstream;

auto steranoid::extrait_depuis_fichier(ifstream & ifs) -> nombre_tab {
	auto tmp = 0ull;
	auto m = nombre_tab();

	for (auto i = coordinates_type::dimension(); i < 20 and ifs.good();++i) {
		for (auto j = coordinates_type::dimension(); j < 20 and ifs.good(); ++j) {
			auto coords = coordinates_type(i,j);
			ifs >> tmp;
			m[coords] = tmp;
		}
	}

	return m;
}
