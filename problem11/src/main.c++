//This file is part of steranoid_projecteuler.

//steranoid_projecteuler is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//steranoid_projecteuler is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with steranoid_projecteuler.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>
#include <iostream>
#include <fstream>

using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;

#include "produit_alignes.h++"
using steranoid::produit_alignes;

int main(int argc, char**argv) {
	if (argc != 2) {
		cerr << "Usage : " << argv[0] << " NOM_DE_FICHIER" << endl;
		return EXIT_FAILURE;
	}
	auto ifs = ifstream(argv[1], std::ifstream::in);
	auto produit = produit_alignes(ifs);
	cout << "Le plus grand produit de nombres alignés dans cette série est : " << produit << endl;

	return EXIT_SUCCESS;
}
