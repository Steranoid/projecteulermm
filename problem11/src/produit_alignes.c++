#include "produit_alignes.h++"
#include "produit_liste.h++"
using steranoid::produit_liste;
#include "extraction_fichier.h++"
using steranoid::extrait_depuis_fichier;

using std::ifstream;

#include <set>
using std::set;
#include <list>
using std::list;
#include <algorithm>
using std::max;

auto steranoid::produit_alignes(ifstream & ifs, unsigned long size) -> unsigned long long {
	auto produit = 0ull;

	auto const m = extrait_depuis_fichier(ifs);

	for (auto i = m.begin(); i != m.end(); ++i) {
		auto & coords = i->first;

		//direction nord-sud
		auto l = list<unsigned long long>();
		for (auto i = 0ul; i < size; ++i) {
			auto c = coords + i * coordinates<unsigned short>(1,0);
			if (m.find(c) != m.end()) {
				l.push_back(m.at(c));
			}
		}
		produit = std::max(produit_liste(l),produit);
		//direction est-ouest
		l.clear();
		for (auto i = 0ul; i < size; ++i) {
			auto c = coords + i * coordinates<unsigned short>(0,1);
			if (m.find(c) != m.end()) {
				l.push_back(m.at(c));
			}
		}
		produit = std::max(produit_liste(l),produit);
		//direction nordouest-sudest
		l.clear();
		for (auto i = 0ul; i < size; ++i) {
			auto c = coords + i * coordinates<unsigned short>(1,1);
			if (m.find(c) != m.end()) {
				l.push_back(m.at(c));
			}
		}
		produit = std::max(produit_liste(l),produit);
		//direction nordest-sudouest
		l.clear();
		for (auto i = 0ul; i < size; ++i) {
			auto c = coords + i * coordinates<unsigned short>(1,-1);
			if (m.find(c) != m.end()) {
				l.push_back(m.at(c));
			}
		}
		produit = std::max(produit_liste(l),produit);
	}

	return produit;
}
