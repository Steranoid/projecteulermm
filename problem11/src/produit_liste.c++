#include "produit_liste.h++"
using std::list;

auto steranoid::produit_liste(list<unsigned long long> const & q) -> unsigned long long {
	auto produit = 1ull;
	for (auto i = q.begin(); i != q.end(); ++i) {
		produit *= *i;
	}
	return produit;
}
