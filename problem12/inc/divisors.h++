#ifndef DIVISORS_HPP
#define DIVISORS_HPP

#include "prime_iterator.h++"
using steranoid::prime_iterator;

namespace projecteuler {
	unsigned long number_of_divisors(unsigned long, prime_iterator<unsigned long> i = prime_iterator<unsigned long>(0));
}

#endif
