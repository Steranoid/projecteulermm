#ifndef TRIANGLE_NUMBER_HPP
#define TRIANGLE_NUMBER_HPP

namespace projecteuler {
	inline unsigned long triangle_number(unsigned long n) {
		return n*(n+1)/2;
	}
}

#endif
