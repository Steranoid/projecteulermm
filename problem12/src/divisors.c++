#include <cmath>
using std::pow;

#include "divisors.h++"

namespace projecteuler {
	unsigned long number_of_divisors(unsigned long n, prime_iterator<unsigned long> i) {
		//if n == 0, then return 0, else return the number of divisors
		auto res = 0ul;

		//if n is 0, it has an inifinite number of divisors ... so we return 0 to express that (because no numbers have 0 divisors so you will know it's 0
		if (n == 0) {
			res = 0;
		//if n is 1  then the only divisor is 1
		} else if (n == 1) {
			res = 1;
			//if n is prime, then it only has two divisors (itself and one), note that all possible prime factor of n are calculated in the process and will populate the shared vector of primes of i, that way, we can use it if it's not prime
		} else if (prime_iterator<unsigned long>::is_prime(n, i)) {
			res = 2;
		//if it's not, we have to work more on it, knowing that all possible prime factor of n are already calculated now
		} else {
			for (i = 0ul; *i < n; ++i) {
				auto tmp = 0ul;
				for (auto j = 1; n % (unsigned long)pow(*i,j) == 0;++j) {
					tmp = res * j + j;
				}
				res += tmp;
			}
			res += 1;
		}

		return res;
	}
}
