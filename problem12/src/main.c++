//This file is part of steranoid_projecteuler.

//steranoid_projecteuler is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//steranoid_projecteuler is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with steranoid_projecteuler.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>
#include <iostream>

using std::cout;
using std::endl;

#include "divisors.h++"
using namespace projecteuler;
#include "triangle_number.h++"

int main(int, char**) {
	steranoid::prime_iterator<long unsigned> i;

	unsigned long Dn;
	unsigned long n;

	for (n = 1, Dn = 1;Dn <= 500; ++n) {
		if (n % 2 == 0)
			Dn = number_of_divisors((n/2), i) * number_of_divisors(n+1, i);
		else
			Dn = number_of_divisors(n, i) * number_of_divisors((n+1)/2, i);
	}

	cout << triangle_number(n-1) << " a un nombre de diviseur supérieur à 500" << endl;

	return EXIT_SUCCESS;
}
