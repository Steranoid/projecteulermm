//This file is part of steranoid_projecteuler.

//steranoid_projecteuler is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//steranoid_projecteuler is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with steranoid_projecteuler.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::fstream;
using std::string;
using std::vector;

const string nomfichier = "res/fichier.txt";

void ajoute_et_affiche(vector<string> const & vec, unsigned long i, unsigned long n = 0) {
	for (auto s : vec) {
		n += s[i]-'0';
	}
	if (i == 0) {
		cout << n;
	} else {
		ajoute_et_affiche(vec, i-1, n/10);
		cout << n%10;
	}
}

int main(int, char**) {
	fstream fichier(nomfichier);
	if (not fichier.is_open()) {
		cout << "Ouverture du fichier impossible" << endl;
		return EXIT_FAILURE;
	} else {
		string ligne;
		vector<string> lignes;

		while (fichier >> ligne) {
			lignes.push_back(ligne);
		}

		ajoute_et_affiche(lignes, 49);
		cout << endl;
	}
	
	return EXIT_SUCCESS;
}
