#ifndef COLLATZ_HPP
#define COLLATZ_HPP

#include <tuple>

namespace projecteuler {
	unsigned long nb_collatz_sequence(unsigned long n) {
		unsigned long answer;
		for (answer = 0;n!=1; ++answer) {
			if (n % 2 == 0)
				n = n/2;
			else
				n = 3*n+1;
		}
		return answer+1;
	}

	std::tuple<unsigned long, unsigned long> plus_longue_sequence_collatz(unsigned long i) {
		unsigned long best = 1ul, nb = 1ul;

		for (--i; i > 1; --i) {
			if (auto tmp = nb_collatz_sequence(i); tmp > nb) {
				nb = tmp;
				best = i;
			}
		}

		return {best, nb};
	}
}

#endif
