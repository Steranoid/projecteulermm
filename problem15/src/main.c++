//This file is part of steranoid_projecteuler.

//steranoid_projecteuler is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//steranoid_projecteuler is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with steranoid_projecteuler.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>
#include <iostream>
#include <map>
#include <memory>

using std::cout;
using std::endl;
using std::map;
using std::shared_ptr;
using std::make_shared;

#include "coordinates.h++"
using namespace projecteuler;

struct move {
	coordinates<unsigned long> source;
	coordinates<unsigned long> destination;

	inline bool operator==(move a) const {
		return source == a.source and destination == a.destination;
	}
	inline bool operator!=(move a) const {
		return not operator==(a);
	}
};

struct move_sorter {
	bool operator()(const move & a, const move & b) const {
		return (a.source.x() < b.source.x()                                                                                                                       ) or
		       (a.source.x() == b.source.x() and a.source.y() < b.source.y()                                                                                      ) or
		       (a.source.x() == b.source.x() and a.source.y() == b.source.y() and a.destination.x() < b.destination.x()                                           ) or
		       (a.source.x() == b.source.x() and a.source.y() == b.source.y() and a.destination.x() == b.destination.x() and a.destination.y() < b.destination.y());
	}
};

unsigned long trouve_nombre_de_chemin(coordinates<unsigned long> source, coordinates<unsigned long> destination, shared_ptr<map<move, unsigned long, move_sorter>> moves = make_shared<map<move, unsigned long, move_sorter>>()) {
	cout << "Nombre de chemin entre " << source.x() << ';' << source.y() << " et " << destination.x() << ';' << destination.y() << endl;
	unsigned long resultat;
	if (moves->count(move{source, destination}) == 1) {
		cout << "Chemin trouvé !" << endl;
		resultat = moves->at(move{source, destination});
	} else {
		if (source != destination and (source.x() == destination.x() or source.y() == destination.y())) {
			resultat = 1ul;
		} else {
			resultat = 0ul;
			auto deplacement_sud = source+coordinates{1ul, 0ul};
			auto deplacement_est = source+coordinates{0ul, 1ul};
			if (deplacement_sud.x() <= destination.x() and deplacement_sud.y() <= destination.y()) {
				resultat += trouve_nombre_de_chemin(deplacement_sud, destination, moves);
			}
			if (deplacement_est.x() <= destination.x() and deplacement_est.y() <= destination.y()) {
				resultat += trouve_nombre_de_chemin(deplacement_est, destination, moves);
			}
		}
		(*moves)[move{source, destination}] = resultat;
	}
	return resultat;
}

int main(int, char**) {
	cout << "Nombre de chemins disponibles entre {0,0} et {2,2} : ";
	coordinates source{0ul,0ul};
	coordinates destination{2ul,2ul};
	auto nombre_de_chemins = trouve_nombre_de_chemin(source, destination);
	cout << nombre_de_chemins << endl;
	cout << "Nombre de chemins disponibles entre {0,0} et {20,20} : ";
	source = {0ul,0ul};
	destination = {20ul,20ul};
	nombre_de_chemins = trouve_nombre_de_chemin(source, destination);
	cout << nombre_de_chemins << endl;
	return EXIT_SUCCESS;
}
