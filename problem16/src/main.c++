//This file is part of steranoid_projecteuler.

//steranoid_projecteuler is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//steranoid_projecteuler is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with steranoid_projecteuler.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::string;
using std::vector;

unsigned long somme_des_chiffres(string nombre) {
	unsigned long somme = 0;
	for (auto c : nombre) {
		somme += (c - '0');
	}
	return somme;
}

string somme_de_chaines(string s1, string s2) {
	string & plus_longue = (s1.size() > s2.size()) ? s1 : s2;
	string & plus_courte = (s1.size() <= s2.size()) ? s1 : s2;
	string resultat;
	unsigned long i = 0, r = 0;

	for (unsigned long a = 0, b = 0, c = 0; i < plus_longue.size() and i < plus_courte.size(); ++i) {
		a = plus_longue[plus_longue.size()-1-i] - '0';
		b = plus_courte[plus_courte.size()-1-i] - '0';
		c = a + b + r;
		resultat = char('0'+(c%10)) + resultat;
		r = c/10;
	}
	for (unsigned long a = 0, c = 0; i < plus_longue.size(); ++i) {
		a = plus_longue[plus_longue.size()-1-i] - '0';
		c = a + r;
		resultat = char('0'+(c%10)) + resultat;
		r = c/10;
	}
	for (;r != 0;r = r/10) {
		resultat = char('0'+(r%10)) + resultat;
	}

	return resultat;
}

string somme_de_chaines(vector<string> strings) {
	string resultat;
	for (auto i : strings) {
		if (resultat.size() == 0)
			resultat = i;
		else
			resultat = somme_de_chaines(resultat, i);
	}
	return resultat;
}

string multiplication_de_chaines(string s1, string s2) {
	vector<string> resultats;


	for (auto i = 0ul; i < s1.size(); i++) {
		string resultat;
		unsigned long r = 0ul;
		unsigned long a = s1[s1.size()-1-i]-'0';
		for (auto k = 0ul; k < i; k++) {
			resultat = '0' + resultat;
		}
		for (auto j = 0ul; j < s2.size(); j++) {
			unsigned long b = s2[s2.size()-1-j]-'0';
			unsigned long c = a * b + r;
			resultat = char('0'+(c%10)) + resultat;
			r = c/10;
		}
		for (;r != 0;r = r/10) {
			resultat = char('0'+(r%10)) + resultat;
		}
		resultats.push_back(resultat);
	}
	
	return somme_de_chaines(resultats);
}

string puissance_de_chaines(string a, unsigned long exposant) {
	string reponse = "1";
	for (auto i = 0ul; i < exposant; ++i) {
		reponse = multiplication_de_chaines(reponse,a);
	}

	return reponse;
}

int main(int, char**) {
	auto puissance = puissance_de_chaines("2",1000);
	auto somme = somme_des_chiffres(puissance);
	cout << "Somme des chiffres de 2^1000 (" << puissance << ") : " << somme << endl;
	return EXIT_SUCCESS;
}
