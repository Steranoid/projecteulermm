//This file is part of steranoid_projecteuler.

//steranoid_projecteuler is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//steranoid_projecteuler is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with steranoid_projecteuler.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>
#include <iostream>

using std::cout;
using std::endl;

int main(int, char**) {
	auto somme_des_carres = 0ull, carre_de_somme = 0ull, difference = 0ull;

	for (auto i = 1ull; i <= 100; ++i) {
		somme_des_carres += i*i;
		carre_de_somme += i;
	}
	carre_de_somme *= carre_de_somme;
	difference = carre_de_somme - somme_des_carres;

	cout << "La différence entre le carre de la somme des 100 premiers nombre entier " << carre_de_somme << " et la somme des carrés de ceux-ci " << somme_des_carres << " est de " << difference << endl;
	return EXIT_SUCCESS;
}
