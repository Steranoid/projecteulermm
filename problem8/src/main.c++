//This file is part of steranoid_projecteuler.

//steranoid_projecteuler is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//steranoid_projecteuler is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with steranoid_projecteuler.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <list>
#include <algorithm>

using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
using std::list;
using std::max;

auto produit_liste(list<unsigned long long> const & q) {
	auto produit = 1ull;
	for (auto i = q.begin(); i != q.end(); ++i) {
		produit *= *i;
	}
	return produit;
}

unsigned long long produit_des_treize(ifstream & ifs) {
	auto produit = 0ull, i = 0ull;
	auto c = '0';
	auto q = list<unsigned long long>();
	
	while (ifs.good()) {
		ifs.get(c);
		if (c >= '0' and c <= '9') {
			i =  c - '0';
			q.push_back(i);
			if (q.size() > 13) {
				q.pop_front();
			}
			if (q.size() == 13) {
				produit = max(produit, produit_liste(q));
			}
		}
	}

	ifs.close();

	return produit;
}

int main(int argc, char**argv) {
	if (argc != 2) {
		cerr << "Usage : " << argv[0] << " NOM_DE_FICHIER" << endl;
		return EXIT_FAILURE;
	}
	auto ifs = ifstream(argv[1], std::ifstream::in);
	
	cout << "Le plus grand produit de treize chiffres consécutifs dans cette série est : " << produit_des_treize(ifs) << endl;


	return EXIT_SUCCESS;
}
