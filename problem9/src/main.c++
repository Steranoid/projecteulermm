//This file is part of steranoid_projecteuler.

//steranoid_projecteuler is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//steranoid_projecteuler is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with steranoid_projecteuler.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>
#include <iostream>
#include <sstream>

using std::cout;
using std::cerr;
using std::endl;
using std::istringstream;

bool est_pythagorien(unsigned long long a, unsigned long long b, unsigned long long c) {
	return (a*a + b*b) == (c*c) and a < b and b < c;
}

auto calculate_c(unsigned long long a, unsigned long long b, unsigned long long limit) {
	return  limit - a - b;
}

int main(int argc, char** argv) {
	unsigned long long a, b, c, produit = 0ull, limit;

	if (argc != 2) {
		cerr << "Usage : " << argv[0] << " SOMME_DES_TROIS_NOMBRES" << endl;
		return EXIT_FAILURE;
	}

	istringstream iss(argv[1]);
	iss >> limit;

	for (a = 1ull, b = 2ull, c = calculate_c(a, b, limit);b < c and produit == 0ull; ++b, a = 1ull, c = calculate_c(a, b, limit)) {
		for (a = 1ull;a < b and b < c and produit == 0ull; ++a, c = calculate_c(a, b, limit)) {
			cout << "Pour : " << a << ", " << b << " et " << c << " : ";
			if (est_pythagorien(a, b, c)) {
				produit = a * b * c;
				cout << "Oui ! : " << produit << endl;
			} else {
				cout << "Non ..." << endl;
			}
		}
	}

	return EXIT_SUCCESS;
}
